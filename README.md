# Ben Bals' Dotfiles 

## Notes on installing
- you have to install a powerline font to make zsh work and set it to be used in terminal settings (automizing: TODO)

## What's includede
- zsh config
- script to do symlinking

## Todo
- [ ] Write better symlinking script
- [ ] Move emacs config over
- [ ] Write Brewfile
- [ ] nvm
- [ ] install xcode tools
