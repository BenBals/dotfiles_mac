export LANG=de_DE.UTF-8
export LC_COLLATE="C"
export LC_CTYPE="de_DE.UTF-8"
export LC_MESSAGES="C"
export LC_MONETARY="C"
export LC_NUMERIC="C"
export LC_TIME="C"
