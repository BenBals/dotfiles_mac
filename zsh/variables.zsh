export CODE=$HOME/code

# Go stuff
export GOPATH=$HOME/code/Go
# needs go to be installed via brew
export GOROOT=/usr/local/opt/go/libexec
export PATH=$PATH:$GOPATH/bin
export PATH=$PATH:$GOROOT/bin

# Add yarn stuff to path
export PATH="$PATH:`yarn global bin`"
