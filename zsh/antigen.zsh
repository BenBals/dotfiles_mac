### config antigen
source /Users/beb/.dotfiles/zsh/antigen/antigen.zsh
antigen use oh-my-zsh

### adding antigen plugins
antigen bundle git
antigen bundle heroku
antigen bundle pip
antigen bundle lein
antigen bundle command-not-found
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle soimort/you-get
antigen bundle fasd

antigen theme bhilburn/powerlevel9k powerlevel9k


