export PATH=$PATH:/Library/TeX/texbin

# Android Stuff
export ANDROID_HOME=/Library/Android/SDK
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$ANDROID_HOME/platform_tools:$PATH

# Raspberry Pi Toolchain
export PATH=$PATH:/usr/local/linaro/arm-linux-gnueabihf-raspbian/bin

# Cabel stuff
export PATH="$HOME/Library/Haskell/bin:$PATH"
