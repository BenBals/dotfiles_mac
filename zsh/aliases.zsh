alias reload!='. ~/.zshrc'

alias cls='clear' # Good 'ol Clear Screen command

alias vita='cd ~/code/vitabox/vitaprog'

alias e="emacs ."
alias n="nvim"

alias y="yarn"
alias yr="yarn run"
alias ys="yarn start"
